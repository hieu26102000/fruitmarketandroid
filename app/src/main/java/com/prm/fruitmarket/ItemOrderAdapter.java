package com.prm.fruitmarket;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ItemOrderAdapter extends RecyclerView.Adapter<ItemOrderAdapter.ItemOrderViewHolder>{

    private Context mContext;
    private List<ItemOrder> mListItemOrder;

    public ItemOrderAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setData(List<ItemOrder> list) {
        this.mListItemOrder = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_order,parent,false);
        return new ItemOrderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemOrderAdapter.ItemOrderViewHolder holder, int position) {
        ItemOrder itemOrder = mListItemOrder.get(position);
        if(itemOrder == null) {
            return;
        }
        holder.img_item.setImageResource(itemOrder.getRecourseId());
        holder.date.setText(itemOrder.getDate());
    }

    @Override
    public int getItemCount() {
        if (mListItemOrder != null) {
            return mListItemOrder.size();
        }
        return 0;
    }

    public class ItemOrderViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView img_item;
        private TextView date;

        public ItemOrderViewHolder(View itemView) {
            super(itemView);
            img_item = itemView.findViewById(R.id.img_item);
            date = itemView.findViewById(R.id.tv_date);
        }
    }
}
