package com.prm.fruitmarket;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.prm.fruitmarket.firebase.model.User;
import com.prm.fruitmarket.utils.ValidateHelper;

import java.util.Calendar;

public class RegisterActivity extends AppCompatActivity {

    private String TAG = "hieu";

    private FirebaseFirestore db;
    private CollectionReference userCollection;

    private LoadingDialog loadingDialog;

    Button btnRegister;
    EditText editTextName, editTextEmail, editTextMobile, editTextPassword, editTextDOB, editTextAddress;
    TextInputLayout textInputDOB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextMobile = (EditText) findViewById(R.id.editTextMobile);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextDOB = (EditText) findViewById(R.id.editTextDOB);
        editTextAddress = (EditText) findViewById(R.id.editTextAddress);
        textInputDOB = (TextInputLayout) findViewById(R.id.textInputDOB);


        loadingDialog = new LoadingDialog(RegisterActivity.this);
        db = FirebaseFirestore.getInstance();
        userCollection = db.collection("users");

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        editTextDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month + 1;
                        String date = day +  "/" + month + "/" + year;
                        Log.d("date",day +  "/" + month + "/" + year );
                        editTextDOB.setText(date);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });
    }

    public void onLoginClick(View view) {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void returnLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
    }


    private void addNewUser(User user) {
        loadingDialog.startLoading();
        DocumentReference doc = userCollection.document();
        user.setId(doc.getId());
        String email = editTextEmail.getText().toString();


        if (editTextName.getText().toString().isEmpty() || editTextAddress.getText().toString().isEmpty() || editTextDOB.getText().toString().isEmpty()
                || editTextEmail.getText().toString().isEmpty() || editTextMobile.getText().toString().isEmpty() || editTextPassword.getText().toString().isEmpty()) {
            Toast.makeText(RegisterActivity.this, "You must fill all fields", Toast.LENGTH_LONG).show();
            loadingDialog.dismissDialog();
        } else if (!ValidateHelper.isValidEmail(email)) {
            Toast.makeText(this, "Email is not valid", Toast.LENGTH_SHORT).show();
            loadingDialog.dismissDialog();
        } else {
            doc.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void unused) {

                    Log.d(TAG, "Add new user successfully!" + user.getId());
                    Toast.makeText(RegisterActivity.this, "Register Successfully", Toast.LENGTH_LONG).show();
                    loadingDialog.dismissDialog();
                    returnLoginActivity();


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.w(TAG, "Error writing document", e);
                    Toast.makeText(RegisterActivity.this, "Register Fail, please try again", Toast.LENGTH_LONG).show();
                    loadingDialog.dismissDialog();
                }
            });
        }


    }

    public void onRegiterSubmit(View view) {
        String password = editTextPassword.getText().toString();
        User user = new User(editTextEmail.getText().toString(), editTextName.getText().toString(), password,
                editTextDOB.getText().toString(), editTextMobile.getText().toString(), editTextAddress.getText().toString(), "");
        this.addNewUser(user);
    }
}