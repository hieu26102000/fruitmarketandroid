package com.prm.fruitmarket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.firebase.model.CartDetail;
import com.prm.fruitmarket.firebase.model.Product;

import java.util.List;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderDetailViewHolder>{

    private Context context;
    private List<CartDetail> mCartDetail;

    public OrderDetailAdapter(Context context, List<CartDetail> mCartDetail) {
        this.context = context;
        this.mCartDetail = mCartDetail;
    }

    @NonNull
    @Override
    public OrderDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_order,parent,false);
        return new OrderDetailAdapter.OrderDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderDetailAdapter.OrderDetailViewHolder holder, int position) {
        CartDetail cartDetail = mCartDetail.get(position);
        if (cartDetail == null){
            return;
        }
        Glide.with(context).load(cartDetail.getProduct().getImages().get(0).toString()).override(300,300).into(holder.img_product);
        holder.tv_pd_name.setText("Name: " + String.valueOf(cartDetail.getProduct().getName()));
        holder.tv_price.setText("Price: " + String.format("%.0f", cartDetail.getProduct().getPrice()) + "VND");
        holder.tv_quantity.setText("Quantity: " + String.valueOf(cartDetail.getQuantity()));
    }

    @Override
    public int getItemCount() {
        if (mCartDetail !=null){
            return mCartDetail.size();
        }
        return 0;
    }

    public class OrderDetailViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_product;
        private TextView tv_pd_name,tv_price,tv_quantity;
        public OrderDetailViewHolder(View itemView) {
            super(itemView);
            img_product = itemView.findViewById(R.id.img_product);
            tv_pd_name = itemView.findViewById(R.id.tv_pd_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
        }
    }
}
