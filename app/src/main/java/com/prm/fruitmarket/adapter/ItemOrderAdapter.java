package com.prm.fruitmarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.prm.fruitmarket.ListOrderActivity;
import com.prm.fruitmarket.ListOrderDetailActivity;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.firebase.model.CartDetail;
import com.prm.fruitmarket.firebase.model.Order;
import com.prm.fruitmarket.firebase.model.OrderDetail;
import com.prm.fruitmarket.firebase.model.Product;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ItemOrderAdapter extends RecyclerView.Adapter<ItemOrderAdapter.ItemOrderViewHolder> {
    private Context context;
    private List<OrderDetail> mOrderDetail;

    public ItemOrderAdapter(Context context, List<OrderDetail> mOrderDetail) {
        this.context = context;
        this.mOrderDetail = mOrderDetail;
    }

    @NonNull
    @Override
    public ItemOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_order,parent,false);
        return new ItemOrderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemOrderAdapter.ItemOrderViewHolder holder, int position) {
        OrderDetail orderDetail = mOrderDetail.get(position);
        if (orderDetail == null){
            return;
        }
        holder.tv_date.setText(orderDetail.getCreateAt());
        holder.tv_item.setText("Item: " + String.valueOf(orderDetail.getTotalItem()));
        holder.tv_total.setText("Total: " + String.format("%.0f", orderDetail.getTotalMoney()));
    }

    @Override
    public int getItemCount() {
        if (mOrderDetail !=null){
            return mOrderDetail.size();
        }
        return 0;
    }

    public class ItemOrderViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_date, tv_item, tv_total;
        private CardView order_item;

        public ItemOrderViewHolder(View itemView) {
            super(itemView);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_item = itemView.findViewById(R.id.tv_item);
            tv_total = itemView.findViewById(R.id.tv_total);
            order_item = itemView.findViewById(R.id.order_item);
            order_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrderDetail orderDetail = mOrderDetail.get(getAdapterPosition());
                    Intent intent = new Intent(context,ListOrderDetailActivity.class);
                    intent.putExtra("orderDetail",(Serializable)orderDetail);
                    context.startActivity(intent);
                }
            });
        }
    }
}
