package com.prm.fruitmarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prm.fruitmarket.DetailProductActivity;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.firebase.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyProductViewHolder> {
    public List<Product> listProduct;
    public Context context;

    public ProductAdapter(List<Product> listProduct, Context context) {
        this.listProduct = listProduct;
        this.context = context;
    }

    @NonNull
    @Override
    public MyProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.product_item_row, parent, false);
        return new MyProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyProductViewHolder holder, int position) {
        Glide.with(context).load(listProduct.get(position).getImages().get(0).toString()).override(300,300).into(holder.imv);
        holder.productInformation.setText(listProduct.get(position).getName() + " " + String.valueOf(String.format("%.0f", listProduct.get(position).getPrice())));
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public void filterList(ArrayList<Product> filterList)
    {
        listProduct = filterList;
        notifyDataSetChanged();
    }


    public class MyProductViewHolder extends RecyclerView.ViewHolder {
        ImageView imv;
        TextView productInformation;
        String ProductID;
        CardView productCardView;

        public MyProductViewHolder(@NonNull View itemView) {
            super(itemView);
            imv = itemView.findViewById(R.id.productImage);
            productInformation = itemView.findViewById(R.id.productInfor);
            productCardView = itemView.findViewById(R.id.productCardView);

            productCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product p = listProduct.get(getAdapterPosition());
                    Intent intent = new Intent(context, DetailProductActivity.class);
                    intent.putExtra("detailProduct", p);
                    intent.putExtra("images", p.getImages().get(0));
                    intent.putExtra("id", p.getId());
                    context.startActivity(intent);
                }
            });
        }

    }
}
