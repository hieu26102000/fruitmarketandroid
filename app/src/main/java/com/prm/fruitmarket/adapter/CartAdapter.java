package com.prm.fruitmarket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.firebase.model.CartDetail;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyCartViewAdapter> {

    public List<CartDetail> listCartDetails;
    public Context context;
    private ArrayList<CartDetail> listUpdated;
    private boolean updateStatus;

    public CartAdapter(List<CartDetail> listCartDetail, Context context) {
        this.listCartDetails = listCartDetail;
        this.context = context;
        listUpdated = (ArrayList<CartDetail>) listCartDetails;
        updateStatus = false;
    }

    @NonNull
    @Override
    public MyCartViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.cart_item_row, parent, false);
        return new MyCartViewAdapter(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapter.MyCartViewAdapter holder, int position) {
        Glide.with(context).load(listCartDetails.get(position).getProduct().getImages().get(0).toString()).into(holder.imv);
        holder.cartItemName.setText(listCartDetails.get(position).getProduct().getName());
        holder.quantity.setText(String.valueOf(listCartDetails.get(position).getQuantity()));
    }

    public ArrayList<CartDetail> getListUpdated() {
        if (listUpdated != null)
            return listUpdated;
        else
            return new ArrayList<CartDetail>();
    }

    public boolean getUpdateStatus()
    {
        return updateStatus;
    }

//    public void resetCartUpdated()
//    {
//        listUpdated = (ArrayList<CartDetail>) listCartDetails;
//    }
//
//    public void updateOriginalCart()
//    {
//        listCartDetails = listUpdated;
//        notifyDataSetChanged();
//    }
    @Override
    public int getItemCount() {
        return listCartDetails.size();
    }

    public class MyCartViewAdapter extends RecyclerView.ViewHolder {

        ImageView imv;
        TextView cartItemName;
        EditText quantity;
        ImageButton deleteButton;
        ImageButton quantiyUpButton;
        ImageButton quantityDownButton;

        public MyCartViewAdapter(@NonNull View itemView) {
            super(itemView);
            imv = itemView.findViewById(R.id.cartView);
            cartItemName = itemView.findViewById(R.id.cartViewProductName);
            quantity = itemView.findViewById(R.id.quantity);
            deleteButton = itemView.findViewById(R.id.deleteButton);
            quantityDownButton = itemView.findViewById(R.id.removeQuantityButton);
            quantiyUpButton = itemView.findViewById(R.id.addQuantityButton);

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeAt(getAdapterPosition());
                    updateStatus = true;
                }
            });

            quantiyUpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int number = updateQuantity(getAdapterPosition(), true);
                    quantity.setText(String.valueOf(number));
                    updateStatus = true;
                }
            });

            quantityDownButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int number = updateQuantity(getAdapterPosition(), false);
                    quantity.setText(String.valueOf(number));
                    updateStatus = true;
                    //quantity.setText(number);
                }
            });
        }

        public void removeAt(int position) {
            if (position == 0 && listUpdated.size() == 0)
            {
                listUpdated = new ArrayList<>();
                notifyDataSetChanged();
            }
            else
            {
                listUpdated.remove(position);
                notifyItemRangeRemoved(position, listCartDetails.size());
                notifyItemRangeChanged(position, listCartDetails.size());
                notifyDataSetChanged();
            }
        }

        public int updateQuantity(int postion, boolean status) {
            if (status == true) {
                int number = listUpdated.get(postion).getQuantity() + 1;
                listUpdated.get(postion).setQuantity(number);
                notifyItemChanged(postion);
                return number;
            } else {
                int number = listUpdated.get(postion).getQuantity() - 1;
                if (number <= 1)
                {
                    number = 1;
                }
                listUpdated.get(postion).setQuantity(number);
                notifyItemChanged(postion);
                return number;
            }
        }
    }
}
