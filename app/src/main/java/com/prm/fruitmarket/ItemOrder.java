package com.prm.fruitmarket;

public class ItemOrder {
    private int recourseId;
    private String date;

    public ItemOrder(int recourseId, String date) {
        this.recourseId = recourseId;
        this.date = date;
    }

    public int getRecourseId() {
        return recourseId;
    }

    public void setRecourseId(int recourseId) {
        this.recourseId = recourseId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
