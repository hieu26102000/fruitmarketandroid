package com.prm.fruitmarket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.prm.fruitmarket.firebase.model.User;

public class LoginActivity extends AppCompatActivity {

    private String TAG = "hieu";

    private FirebaseFirestore db;
    private CollectionReference userCollection;

    private LoadingDialog loadingDialog;

    SharedPreferences mPrefs;

    Button btnLogin;
    EditText editTextEmail, editTextPassword;

    public LoginActivity() {

    }

    private void authentication(String email, String password) {
        loadingDialog.startLoading();
        userCollection.whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(Task<QuerySnapshot> task) {
                        User user = new User();
                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty()) {
                                Log.d(TAG, "This user with email " + email + " doesn't exist!");
                                clear();
                                Toast.makeText(LoginActivity.this, "This user with email " + email + " doesn't exist!", Toast.LENGTH_SHORT).show();
                                loadingDialog.dismissDialog();
                                return;
                            }

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                user = snapshot.toObject(User.class);
                                Log.d(TAG, "onComplete: " + user.getAvatar());
                            }
                            if (user.getPassword().equals(password) && user.getPassword() != null && user != null && user.getPassword().equals(editTextPassword.getText().toString()) && editTextPassword.getText() != null) {
                                SharedPreferences.Editor prefsEditor = mPrefs.edit();
                                Gson gson = new Gson();
                                String json = gson.toJson(user);
                                prefsEditor.putString("USER", json);
                                prefsEditor.apply();
                                Toast.makeText(LoginActivity.this, "Login Successfully!!! ", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                            } else {
                                clear();
                                Log.d(TAG, "Authentication fail for email! " + email);
                                Toast.makeText(LoginActivity.this, "Login Fail, please try again ", Toast.LENGTH_SHORT).show();
                            }

                            loadingDialog.dismissDialog();
                        } else {
                            clear();
                            Log.d(TAG, "Error getting user: ", task.getException());
                            loadingDialog.dismissDialog();
                            Toast.makeText(LoginActivity.this, "Login Fail, please try again ", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);


        mPrefs = getSharedPreferences("filename", MODE_PRIVATE);
        loadingDialog = new LoadingDialog(LoginActivity.this);
        db = FirebaseFirestore.getInstance();
        userCollection = db.collection("users");

    }

    public void clear() {
        editTextEmail.setText("");
        editTextPassword.setText("");
    }

    public void handleLogin() {
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "You must fill all fields", Toast.LENGTH_SHORT).show();
        } else {
            authentication(email, password);
        }
    }

    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    public void onLoginClick(View View) {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    public void onLoginSubmit(View view) {
        handleLogin();
//        this.authentication(editTextEmail.getText().toString(), editTextPassword.getText().toString());
    }
}