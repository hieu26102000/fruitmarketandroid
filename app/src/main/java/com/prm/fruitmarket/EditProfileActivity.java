package com.prm.fruitmarket;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.prm.fruitmarket.firebase.model.User;
import com.prm.fruitmarket.fragment.ProfileFragment;

import java.util.Calendar;

public class EditProfileActivity extends AppCompatActivity {

    private TextView txt_backPressed;
    private User user;
    private String TAG = "hieu";
    private FirebaseFirestore db;
    private CollectionReference userCollection;
    private LoadingDialog loadingDialog;
    private SharedPreferences mPrefs;
    private EditText edt_name, edt_mail, edt_password, edt_dob, edt_phone, edt_address;
    private Button btn_change;
    private User newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        loadingDialog = new LoadingDialog(EditProfileActivity.this);
        db = FirebaseFirestore.getInstance();
        userCollection = db.collection("users");
        mPrefs = getSharedPreferences("filename", MODE_PRIVATE);

        Gson gson = new Gson();
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);
        getMyInfo(user.getEmail());

    }
    private void loadInfo() {
        //        fill data into view
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_mail = (EditText) findViewById(R.id.edt_mail);
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_dob = (EditText) findViewById(R.id.edt_dob);

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        edt_phone = (EditText) findViewById(R.id.edt_phone);
        edt_address = (EditText) findViewById(R.id.edt_address);

        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(EditProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month + 1;
                        String date = day +  "/" + month + "/" + year;
                        edt_dob.setText(date);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        showAllUserData();
//        click event button change
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserData();
                updateProfile(newUser);
            }
        });

        txt_backPressed = findViewById(R.id.txt_backPressed);
        txt_backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
    private void getMyInfo(String email) {
        loadingDialog.startLoading();
        userCollection.whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty()) {
                                Log.d(TAG, "This user with email " + email + " doesn't exist!");
                                loadingDialog.dismissDialog();
                                return;
                            }

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                user = snapshot.toObject(User.class);
                            }
                            loadInfo();

                        } else {
                            Log.d(TAG, "Error getting user: ", task.getException());
                        }
                        loadingDialog.dismissDialog();
                    }
                });
    }
    //show user data
    private void showAllUserData() {
        edt_name.setText(user.getFullname().trim());
        edt_mail.setText(user.getEmail().trim());
        edt_password.setText(user.getPassword().trim());
        edt_dob.setText(user.getDOB().trim());
        edt_phone.setText(user.getPhone().trim());
        edt_address.setText(user.getAddress().trim());
    }

    //save new user data
    private void saveUserData(){

        newUser = new User();

        newUser.setFullname(edt_name.getText().toString());
        newUser.setEmail(edt_mail.getText().toString());
        newUser.setPassword(edt_password.getText().toString());
        newUser.setDOB(edt_dob.getText().toString());
        newUser.setPhone(edt_phone.getText().toString());
        newUser.setAddress(edt_address.getText().toString());
        newUser.setAvatar(user.getAvatar());
    }

    //update user infor
    private void updateProfile(User newUser) {
        loadingDialog.startLoading();
        newUser.setId(this.user.getId());
        DocumentReference userRef = userCollection.document(user.getId());
        userRef.set(newUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Log.d(TAG, "Update profile successfully!");
                loadingDialog.dismissDialog();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w(TAG, "Error updating profile", e);
                loadingDialog.dismissDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}