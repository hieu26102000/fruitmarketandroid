package com.prm.fruitmarket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.prm.fruitmarket.adapter.ItemOrderAdapter;
import com.prm.fruitmarket.firebase.model.Order;
import com.prm.fruitmarket.firebase.model.OrderDetail;
import com.prm.fruitmarket.firebase.model.User;
import com.prm.fruitmarket.fragment.ProfileFragment;

import java.util.ArrayList;
import java.util.List;

public class ListOrderActivity extends AppCompatActivity {

    private RecyclerView rcvItemOrder;
    private com.prm.fruitmarket.adapter.ItemOrderAdapter ItemOrderAdapter;
    private TextView backPressed;

    private String TAG = "hieu";
    private FirebaseFirestore db;
    private CollectionReference orderCollection;

    private LoadingDialog loadingDialog;
    private User user;
    SharedPreferences mPrefs;
    private ArrayList<ItemOrder> itemOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_order);

        loadingDialog = new LoadingDialog(ListOrderActivity.this);
        db = FirebaseFirestore.getInstance();
        orderCollection = db.collection("orders");

        Gson gson = new Gson();
        mPrefs = getSharedPreferences("filename", Context.MODE_PRIVATE);
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);

        backPressed = findViewById(R.id.tv_backPressedOrder);
        backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rcvItemOrder = findViewById(R.id.rcv_item_order);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcvItemOrder.setLayoutManager(linearLayoutManager);
        getListOrder();

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rcvItemOrder.addItemDecoration(itemDecoration);


    }

    private void getListOrder() {
        loadingDialog.startLoading();
        DocumentReference orderRef = orderCollection.document(user.getEmail());
        orderRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        ArrayList<OrderDetail> orderDetails = document.toObject(Order.class).getOrderDetail();
                        // set data len view
                        Log.d(TAG, "Get orders successfully !" + orderDetails.size());

                        ItemOrderAdapter = new ItemOrderAdapter(ListOrderActivity.this, (List) orderDetails);
                        rcvItemOrder.setAdapter(ItemOrderAdapter);
                    } else {
                        Log.d(TAG, "There are no orders!");
                    }
                } else {
                    Log.d(TAG, "get cart fail ", task.getException());
                }
                loadingDialog.dismissDialog();
            }

            ;
        });
    }

    @Nullable
    @Override
    public View onCreatePanelView(int featureId) {
        return super.onCreatePanelView(featureId);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}