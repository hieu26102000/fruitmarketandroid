package com.prm.fruitmarket;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.prm.fruitmarket.firebase.model.Cart;
import com.prm.fruitmarket.firebase.model.CartDetail;
import com.prm.fruitmarket.firebase.model.Order;
import com.prm.fruitmarket.firebase.model.OrderDetail;
import com.prm.fruitmarket.firebase.model.User;
import com.prm.fruitmarket.fragment.CartFragment;
import com.prm.fruitmarket.fragment.ProfileFragment;

import java.util.ArrayList;

public class CheckOutActivity extends AppCompatActivity {
    private Button button;
    private String TAG = "hieu";

    private FirebaseFirestore db;
    private CollectionReference cartCollection;
    private CollectionReference orderCollection;

    private LoadingDialog loadingDialog;
    private User user;
    private int totalItems;
    private float totalMoney;
    private ArrayList<CartDetail> cartDetails;
    ImageView backButton;

    SharedPreferences mPrefs;

    private void checkout() {
        loadingDialog.startLoading();
        DocumentReference cartRef = cartCollection.document(user.getEmail());
        cartRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        cartDetails = document.toObject(Cart.class).getCartDeatil();
                        String address = user.getAddress();
                        totalItems = cartDetails.size();
                        Log.d(TAG, "Size" + cartDetails.size());
                        totalMoney = 0;
                        for (CartDetail cartDetail : cartDetails) {
                            float price = cartDetail.getProduct().getPrice() * cartDetail.getQuantity();
                            totalMoney += price;
                        }

                        setOrderInfo(totalMoney, totalItems, address);
                        loadingDialog.dismissDialog();
                        Log.d(TAG, "Check out info :" + address + " " + totalItems + " " + totalMoney);
                    } else {
                        Log.d(TAG, "Cart for this user doesn't exist ");
                    }
                } else {
                    Log.d(TAG, "get cart fail ", task.getException());
                }
                loadingDialog.dismissDialog();
            }
        });
    }

    public void setOrderInfo(float totalMoney, int totalItems, String address) {
        TextView tvTotalMoney;
        TextView tvTotalItems;
        TextView tvAddress;
        tvTotalMoney = findViewById(R.id.tvTotalMoney);
        tvTotalItems = findViewById(R.id.tvTotalItems);
        tvAddress = findViewById(R.id.tvAddress);
        tvTotalMoney.setText(String.format("%.0f", totalMoney));
        tvTotalItems.setText(totalItems + " items. Delivery time: 30p");
        tvAddress.setText(address);
    }

    public void makeOrder() {
        OrderDetail orderDetail = new OrderDetail(
                totalMoney,
                totalItems,
                cartDetails,
                user.getAddress()
        );
        loadingDialog.startLoading();
        DocumentReference orderRef = orderCollection.document(user.getEmail());
        DocumentReference cartRef = cartCollection.document(user.getEmail());
        orderRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        ArrayList<OrderDetail> orderDetails = document.toObject(Order.class).getOrderDetail();
                        if (cartDetails!= null) {
                            Log.d(TAG, "Cart Detail " + cartDetails.size());
                            orderDetails.add(orderDetail);
                            Order order = new Order(orderDetails);
                            orderRef.set(order)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void unused) {
                                            Log.d(TAG, "Make order successfully!");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.w(TAG, "Error make order", e);
                                        }
                                    });
                        }

                    } else {
                        ArrayList<OrderDetail> orderDetails = new ArrayList();
                        orderDetails.add(orderDetail);
                        Order order = new Order(orderDetails);

                        orderRef.set(order)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(Task<Void> task) {
                                        Log.d(TAG, "Make order successfully for : " + user.getEmail());
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        Log.d(TAG, "Make order fail for : " + user.getEmail());
                                    }
                                });
                    }

                    Cart cart = new Cart(new ArrayList<CartDetail>(), user.getEmail());
                    cartRef.set(cart)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unused) {
                                    Log.d(TAG, "Refresh carts successfully!");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(Exception e) {
                                    Log.w(TAG, "Error updating cart", e);
                                }
                            });
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
                loadingDialog.dismissDialog();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        Intent i = getIntent();
        cartDetails = (ArrayList<CartDetail>) i.getSerializableExtra("cartDetails");
        button = findViewById(R.id.payNowButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });
        mPrefs = getSharedPreferences("filename", MODE_PRIVATE);
        loadingDialog = new LoadingDialog(CheckOutActivity.this);
        db = FirebaseFirestore.getInstance();
        cartCollection = db.collection("carts");
        orderCollection = db.collection("orders");

        Gson gson = new Gson();
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);

        String address = user.getAddress();
        totalItems = cartDetails.size();
        Log.d(TAG, "Size" + cartDetails.size());
        totalMoney = 0;
        for (CartDetail cartDetail : cartDetails) {
            float price = cartDetail.getProduct().getPrice() * cartDetail.getQuantity();
            totalMoney += price;
        }

        setOrderInfo(totalMoney, totalItems, address);

        backButton = (ImageView) findViewById(R.id.backButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    public void openDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).setTitle("Confirm")
                .setMessage("Do you want to check out?")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        makeOrder();
                        Intent intent = new Intent(CheckOutActivity.this, CheckOutSuccessfulActivity.class);
                        startActivity(intent);
                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}