package com.prm.fruitmarket.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.prm.fruitmarket.CheckOutActivity;
import com.prm.fruitmarket.LoginActivity;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.adapter.CartAdapter;
import com.prm.fruitmarket.firebase.model.Cart;
import com.prm.fruitmarket.firebase.model.CartDetail;
import com.prm.fruitmarket.firebase.model.User;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment {
    private String TAG = "hieu";
    private Dialog dialog;

    private FirebaseFirestore db;
    private CollectionReference cartCollection;

    private User user;
    private SharedPreferences mPrefs;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView rc;
    private Button buttonOrder;
    private CartAdapter cartAdapter;

    public CartFragment() {
        // Required empty public constructor
    }

    private void updateCart(ArrayList<CartDetail> cartDetails) {
        String email = user.getEmail();
        dialog.setCancelable(false);
        setDialog(true);
        DocumentReference cartRef = cartCollection.document(email);
        cartRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Cart cart = new Cart(cartDetails, email);
                    cartRef.set(cart)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unused) {
                                    Log.d(TAG, "Add to cart successfully!");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(Exception e) {
                                    Log.w(TAG, "Error updating cart", e);
                                }
                            });
                }
                setDialog(false);
            }
        });
    }

    private void addToCart(CartDetail cartDetail) {
        String email = user.getEmail();
        dialog.setCancelable(false);
        setDialog(true);
        DocumentReference cartRef = cartCollection.document(email);
        cartRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        ArrayList<CartDetail> cartDetails = document.toObject(Cart.class).getCartDeatil();
                        Log.d(TAG, "Cart Detail " + cartDetails.size());
                        cartDetails.add(cartDetail);
                        Cart cart = new Cart(cartDetails, email);
                        cartRef.set(cart)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        Log.d(TAG, "Add to cart successfully!");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        Log.w(TAG, "Error updating cart", e);
                                    }
                                });
                    } else {
                        ArrayList cartDetails = new ArrayList();
                        cartDetails.add(cartDetail);
                        Cart cart = new Cart(cartDetails, email);
                        cartRef.set(cart)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(Task<Void> task) {
                                        Log.d(TAG, "Add new cart successfully for : " + email);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        Log.d(TAG, "Add new cart fail for : " + email);
                                    }
                                });
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
                setDialog(false);
            }
        });
    }

    private void getCart() {
        dialog.setCancelable(false);
        setDialog(true);
        DocumentReference cartRef = cartCollection.document(user.getEmail());
        cartRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        ArrayList<CartDetail> cartDetails = document.toObject(Cart.class).getCartDeatil();
                        //ArrayList<CartDetail> vefifyCart = new ArrayList<>();
                        for (int i = 0; i < cartDetails.size(); i++) {
                            for (int j = i + 1; j < cartDetails.size(); j++) {
                                if (cartDetails.get(i).getProduct().getName().equalsIgnoreCase(cartDetails.get(j).getProduct().getName())) {
                                    CartDetail ca = cartDetails.get(i);
                                    ca.setQuantity(ca.getQuantity() + cartDetails.get(j).getQuantity());
                                    cartDetails.remove(i);
                                    cartDetails.add(i, ca);
                                    cartDetails.remove(j);
                                }
                            }
                        }
                        updateCart(cartDetails);
                        cartAdapter = new CartAdapter(cartDetails, getActivity());
                        rc.setAdapter(cartAdapter);
                        Log.d(TAG, "Cart Size " + cartDetails.size());
                    } else {
                        Log.d(TAG, "Cart for this user doesn't exist ");
                    }
                } else {
                    Log.d(TAG, "get cart fail ", task.getException());
                }
                setDialog(false);
            }
        });
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mPrefs = getActivity().getSharedPreferences("filename", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);

//        user = new User(
//                "hieu26102000@gmail.com",
//                "Nguyen Van Hieu",
//                "hieu123",
//                "26/10/2000",
//                "09656165xx",
//                "test123",
//                "avatar");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(R.layout.progress_dialog);
        dialog = builder.create();
        db = FirebaseFirestore.getInstance();
        cartCollection = db.collection("carts");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        Product product = new Product(
//                "product 1",
//                "des 1",
//                "Viet Nam",
//                100,
//                100,
//                100,
//                Arrays.asList("sup1", "sup2", "sup3"));
//        CartDetail cartDetail = new CartDetail(product, 5);
//
//        this.addToCart("hieu26102000@gmail.com", cartDetail);
////        this.getCart("hieu26102000@gmail.com");
//        return inflater.inflate(R.layout.fragment_cart, container, false);

        View viewRoot = inflater.inflate(R.layout.fragment_cart, container, false);
        this.rc = viewRoot.findViewById(R.id.recyclerViewCart);
        this.rc.setLayoutManager(new LinearLayoutManager(getActivity()));
//        ArrayList<CartDetail> list = new ArrayList<>();
//        Product p1 = new Product("asds","asdsa",12);
//        Product p2 = new Product("asds","asdsa",12);
//        Product p3 = new Product("asds","asdsa",12);
//        CartDetail ca1 = new CartDetail(p1, 5);
//        CartDetail ca2 = new CartDetail(p2, 10);
//        CartDetail ca3 = new CartDetail(p3, 4);
//        list.add(ca1);
//        list.add(ca2);list.add(ca3);
//        CartAdapter cartAdapter = new CartAdapter(list,getActivity());
//        this.rc.setAdapter(cartAdapter);

        this.getCart();

        return viewRoot;
    }

    private void setDialog(boolean show) {
        if (show) dialog.show();
        else dialog.dismiss();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        buttonOrder = view.findViewById(R.id.buttonOrder);
        buttonOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                updateCart(cartAdapter.getListUpdated());
                if (cartAdapter.getListUpdated().size() == 0 || cartAdapter.getListUpdated() == null) {
                    Toast.makeText(getActivity(), "Your cart is empty!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    updateCart(cartAdapter.getListUpdated());
                    Intent intent = new Intent(getActivity(), CheckOutActivity.class);
                    intent.putExtra("cartDetails",(Serializable)cartAdapter.getListUpdated());
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cartAdapter.getListUpdated() != null) {
            updateCart(cartAdapter.getListUpdated());
        }
    }
}