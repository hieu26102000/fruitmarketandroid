package com.prm.fruitmarket.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.adapter.ProductAdapter;
import com.prm.fruitmarket.firebase.model.CartDetail;
import com.prm.fruitmarket.firebase.model.Product;
import com.prm.fruitmarket.firebase.model.User;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private TextView txtName;
    private User user;

    ArrayList<Product> products;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView rc;

    private FirebaseFirestore db;
    private CollectionReference productCollection;

    private String TAG = "hieu";
    private Dialog dialog;
    private ProductAdapter productAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(R.layout.progress_dialog);
        dialog = builder.create();
        db = FirebaseFirestore.getInstance();
        productCollection = db.collection("products");
        products = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewRoot = inflater.inflate(R.layout.fragment_home, container, false);
        this.rc = viewRoot.findViewById(R.id.recyclerViewProduct);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2, GridLayoutManager.VERTICAL, false);
        this.rc.setLayoutManager(gridLayoutManager);
        this.getListProducts();
        return viewRoot;
    }

    private void getListProducts() {
        dialog.setCancelable(false);
        setDialog(true);
        productCollection.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                products.add(document.toObject(Product.class));
                                Log.d("checkProduct", document.toObject(Product.class).toString());
                            }
                            productAdapter = new ProductAdapter(products,getActivity());
                            rc.setAdapter(productAdapter);
                            Log.d(TAG, "Products size => " + products.size());
                        }else {
                            Log.d(TAG, "Error getting products: ", task.getException());
                        }
                        setDialog(false);
                    }
                });
    }

    @Override
    public void onViewCreated(@NonNull  View view, @Nullable  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtName = view.findViewById(R.id.txtUserName);
        SharedPreferences mPrefs = getActivity().getSharedPreferences("filename", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);
        Log.d("hieu", "User " + user.getFullname());
        txtName.setText("Hello, " + user.getFullname());
        EditText searchText = view.findViewById(R.id.searchText);
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void filter(String text)
    {
        ArrayList<Product> listProductFilter = new ArrayList<>();
        for (Product p : products )
        {
            if (p.getName().toLowerCase().contains(text.toLowerCase()))
            {
                listProductFilter.add(p);
            }
        }
        productAdapter.filterList(listProductFilter);
    }

    private void setDialog(boolean show) {
        if (show) dialog.show();
        else dialog.dismiss();
    }
}