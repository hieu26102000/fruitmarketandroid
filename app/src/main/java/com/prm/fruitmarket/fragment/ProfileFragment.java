package com.prm.fruitmarket.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.prm.fruitmarket.CheckOutActivity;
import com.prm.fruitmarket.CheckOutSuccessfulActivity;
import com.prm.fruitmarket.EditProfileActivity;
import com.prm.fruitmarket.ListOrderActivity;
import com.prm.fruitmarket.LoadingDialog;
import com.prm.fruitmarket.LoginActivity;
import com.prm.fruitmarket.MainActivity;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.firebase.model.User;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int PICK_IMAGE = 1;
    private User user, newUser;
    private String TAG = "hieu";
    private FirebaseFirestore db;
    private CollectionReference userCollection;
    private LoadingDialog loadingDialog;
    private SharedPreferences mPrefs;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CircleImageView avatar;
    private ImageView imv_button;
    private TextView editText, myOrder, logOut, tv_name, tv_mail;
    private Dialog dialog;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void getMyInfo(View view, String email) {
        dialog.setCancelable(false);
        setDialog(true);
        userCollection.whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().isEmpty()) {
                                Log.d(TAG, "This user with email " + email + " doesn't exist!");
                                loadingDialog.dismissDialog();
                                return;
                            }

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                user = snapshot.toObject(User.class);
                            }

                            loadInfo(view);

                        } else {
                            Log.d(TAG, "Error getting user: ", task.getException());
                        }
                        setDialog(false);
                    }
                });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = getActivity().getSharedPreferences("filename", Context.MODE_PRIVATE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(R.layout.progress_dialog);
        dialog = builder.create();

        loadingDialog = new LoadingDialog(getActivity());
        db = FirebaseFirestore.getInstance();
        userCollection = db.collection("users");

        Gson gson = new Gson();
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    private void loadInfo(View view) {
        avatar = (CircleImageView) view.findViewById(R.id.imv_avatar);
        if (user.getAvatar() == null) {
            Glide.with(this).load("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhAQEBAQDxAPDw8PDw8PDw8PDw8NFREWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OFxAQFSsdHR0rLS0tLS0tLS0tLSstKy0tKy0tLS0tKy0tLS0tLS0tLTctLTctLTc3KzcrNysrKy0rK//AABEIAKgBLAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBAACBQEGB//EADYQAAIBAwIEAwcDBAEFAAAAAAABAgMEESExBRJBUWFxgQYTIjKRofCxwdEUUnLh8QcVI0Ji/8QAGQEAAwEBAQAAAAAAAAAAAAAAAQIDAAQF/8QAIhEBAQACAQQCAwEAAAAAAAAAAAECEQMEEiExIkEyQlET/9oADAMBAAIRAxEAPwDyWSZB5Jk8TT0xchKFZxaaFsneYM8BX0L2T4221DXJ9JtJ5SPk/sBac03M+sW0cJHqcNtx3XDyyTLwZcheVQtUkLJtse0kh2LI2VTKVJjA5Xq6AYz/ADxBVplactRdm0czhGbWrOUnFbLd+IevUzpnzFZT6Lv9wWtEm0vF7IRm299cvp2G66zp9xK4qdECmhepUa6arp0T7AVLrKXogdxUxu/p+hn1rj6eJO08jeo3yWwWrcafMl65Z5L+pktmOW1zJrDf3SNMmuLYpwzrzR9F++RyEml0a7LY82+JOk8Syl0eeYap8R5llP1yl+xt6bW2lXunrsvuZlW4k9mn6rP0B3V1lP8AujrvujMldReuz7i3LZpiYq37W+fsSN/4teaMi4uc76+Kf3F41X0ZK1WR6u2vvE0ozzqePtrlm3YXTkh8Mk8sWjc01JNM8Txa0cJPsezhV7mZx23Uotm5cO6BhlqvIJnclJaPBEzh064JkmSuSZFoxbJCuSZAK2SJlcnUZmVKkwbizXnQFqlE6OxDuZ2S0FlpdwlWiN8BtHOtBeJscN3TXLUfTvYfh3JTi3u0e1gtDM4LQUYRXgacmepJqacNu6pNgqE02/AlaeE34CnCKqlz+DwC3yM9NLICvILNitaQaEBqSKxqAa8waqYT8yez6Fq1cC6qfXUDObbCxj19EaXY2acrVXovUzbut+d/9DFxPHUxLxtt6v0jn131Nk0Vr3OX08+i8jPuKmeuPPRh2mls36fmBWrnpHBKyqQJSf8AcHp1vETnnx9SnM/IWeBpzitvOpTcqcvijryptSaXVb5+h5i241Vpyw1to87M3YXLiZ/FaEZrnjFJ7tba9x97CPUWVancUsx3ws4esZGDcuUJOL0af1XcS9krl07mMG2o1cwl25t4v66epu+0NHWM1vs/MTKGl1WTOr+dPMGpgqr0yV5iZztKs9n9TY4TUecmBRkbljLlig4hl6barBJYkmjOjUGKFQttJ5jjFHlk8dTPUje9oKGmTzqZycmOq6MMvAykdyCTLojYfa6ZMnCA0ba2TqKHUDTNGdMWqUjQmgM4nbpy7ZVWia3snSXvULzpGl7NwxVQ/HPlC5Xw+qcPXwoYmL2HyryGJHa5SHEp4g/EW9nqbUZN41edAftBccqwi/B7he55m9t34kv3U/U/Vq6i1WqLu5Ty1qKzr50NlkMxXuKgNz0f50A1HqvUlN6SJbU07Qefr9i3ErpQjpvjIvb1NTJ49ctP008kGXUDW6WqXzcnzPPZLXzf1yEo1lLRrH52PNTulnCeJddvl7j9lfU3HMJKbWFLVN58eozWNx20d1+fczb6Dj/J3/uK+3Xf6g6txzLKYKEhKcmD5jtT86AnP1JU64GtB9Cyn4HJ1DNpmZlCcZ9YyUl6PJ7DibU4ZWqx7yLXbT9pHjb1+J6Dgdfnt0m8um5U3/i1lfrj0GFlSBJB6kcNooo7EzQWzhlmtzClCPKvEupmgU/TmM0J6iNKYxTeo5VuMax9Dyzi+x7KvT5omTOw8DZcfcXv0xIoLFDtWya6C/Lghlx2GnIqok5QyRME+03+gDidUQ2CKIO03ebcjsdQUkMWsDrkRuS8aGTR4PRxUTJQpGhZU/iRfHHSGWb2dh8q8hqQpY7IbkXK8h7YVsYXfT0AUqDqWkqcJyjLlck4Nr4jntm9Y+v0DeyGXCae2yOXfzsdXrCV5z2P4nJValtOMspz/wDJOpKUpbYWHt1PZUKK3MuPBYxuHWWE3o1jU3EuVFM9bTnpnXK1BU9n33GKiy2Cpw0f5oT15U34Jc+JPt+xn+0SzTbXTXPU0rqK1My+qpxcWtGuV9Fr4mGPk/HrqSU4ptNtJtdsilRcklKivd/DFZjKUuf4Em2+7eW+2Xg0vaDh81VkkuaLzh4bUkZ9vwqu9mkv/tPRFuOyRPkxtvg9aceq/BGp8T+VSXzevc3rfiDx/Gxi2fBsPmk/eTemixGPoejtbNRj+YJcmvo+Ev2G7tvpk6m/IPGki3uu5HR7S3mWaDSgcUPAwM+4p5G/Z+pyzlDpNaf5R1X2z9TtSCAwXLJSW6eUHbC3vz+eX+ha1WuexziK1U1s4/fJylLC8wCZnM7FgYsJEIGaUh2lIz6bNC3wwwtalosoY/pQPDtzYjA6MfSGXtjVrIybvh3Y9bUpiVaiDKS+w1XkHQa3RRxPSVbUTq2HgcuWM+jTbGwdQ7UsmgH9O+wlxppkZdEZt6YeVI7CIMeZbLj8GaSHbJfEhGDH+GLMjox5N3Tny49PWWS0Q1IBarQK2dibC4/wr32MaNBOG2apRUUakxeRPsm9n77rTlWKbXgArRGJMWqTBTQvUSwwWMJh5/nkArT6IGjM2+T3wjEupRy86tba8qS8DYvY5zq/rg8zf1ejWndf7JZVTF5zjHDm6nvId1nM859Hqxyyqacsl6Pozs5675X0ZTPVCzI9ng5KEOi6nFLsgMNdQ9NpGt2VZI7hFc57lsGAKSKhpLwByMyjiB5dQ7WSQgAXKlPMeV+a8xfGMLsNVpJIUzkDC0w0QMEHgggJEbtmKpBqQQbvD56m7CawebspGrTrAy5Zg0w2ekwMolY1S6ZHLn2acYTpE9whiKLqBG8lP2QhK0TBPh6NTlLKCNOat/nGHOIFoPIFI5e6raUUjf4FQ69zCpRy0j2PCaWIo7+jxuWW79Ic9kjTprCLNnCsmeq4VZsXlILOQrVlo2LTRSpW3M6vd4KXdzha9THub7L8EJqqRqyvc6eCFa14kvD7mHX4gu6E6nEW3yrVvOvktG/0Fy2eSNi5uFJb+i+x53iKxsv0HXrDGcya1xrjw9RCbaWJLvvlv7aErDxmVMlaVRphK6XT6Y2+4v4iaNs/CQeLEacx2in1089DBRYRC8p2mDqMYtVkzijkJGkF5MGAFxwSNPIRQbC8mFsYSFeiwKpjskmU92BgowCRQWFIv7sLBoJA5yF4owNOxRowELBaGhE5Oo/JXj9CxDQBQDROdQaIRFIBEg0ESCJFYoKkBnnpoDJDUognEnIfaWcfiR7Lh60R5G1j8SPX2C0R6fQ+q5eoOMDMLIDNnoOUKYFx0YSbK03leoBea4jFTly7cq1/PoYlWz5YynJvGG9mJf8AUejWo1VWpVZRU44cU2sPb+Dw95x25lTcJ1ZuOzTeunRstLJPRbL/AFL/AIxOM3h8yzph4DWvGYScfmbljK7HlXWi3832f8Ghwa3cqil0ysb7dyWXlSWvdUrictcOEemWtfHArcV0t2s95YG7flS1cW3yrdLyWvX7A3RinzNtavGWRyxVmRSM4vVty8lyr6v+CSnHpH0bk/0wFqVFrGGvjh4z6A50tlvLs8/Zk7DSuRk+i5fJPP13DUqU98lrWGPPttqOxXdCaHakW0MUo5C06KYaFIYoexI0Wx2nbZGYW+A6DZOlQK16OR6WEDlqbTMj3OC6pjjgDlAGh2AoHGgkkweH1NplWyRL4RaJgaVjsPxEbFo0YxOTqZ5lW4l4oNBFIoLBHMqJALEpBBVEwLRQVIrFBEggw5RBOI3KINxEM5ZU/iR6y0jhI87w+n8R6a3Wh6fRT47cnPfK0xeow9UUmztc4NaRWynnmj6pFLhmfC4cakX3ev8AiLbo0jP9sKUnF8snGccuDXf+D5Lxe3cpOUoY5sKcYxSTkl80caJ+B9245bKcG1vj7M+ccUtE3KKWXpstllfnkim2kfO6XCMPnjFVI7LKbSfijasrbGy6arRam7QVOK93P4V/ck8Kb6ePUIrDD5liUc5yts+np9Tb8NoC3TSytcy2aTaTeGlqXhaRx8XfmjlrK/f/AJCTyljRayWfVb9+oKTjplt/2vOHn80J1SOTrr4eSOc6PGia/kVrRxhLXOzb1a/ktK4/9eXTbGMNPw7BaVs5LX4nutFr39SVNBrLOEpa9s408DRpU1sctbV4zy6m1w7hibTloJrdEG2spdsmhR4a+qPVWFrFRWi2L1bfwLTjSubzLtsC9RG9cW+5mVqALjppWLXbJTix10DrpCaU2ScCkqI/7s5KAdBtnOmVdMan4IDNPsZgXSRzkR2afYos9haJ2z3NiETFs46m7Sjoc/UT4qcftaMQkURIukcSy0EGiisEFijAtFF0cRZBBmOINxGXEoo6iGNcOpG7TWgjYUsJGikez0+PbhHFyXdCqiVRD1VCdRF6mTrIxrn5jZuGYlxLLJZ3SmMHo8Rai4y1WWl3x0M73FNttLd65CcpxI4eTqbLp0Y8U9lavBqU3nCBvgij8vNHHZvD8x1BoVJaajcfWf2Blw/ysOvw2WHmHNnHg9wEeC8yUeWa101TPc8OSl8yTNaFtBbRSO3G903EL8bp4GPsq22+WXxavON92O2/s5yYeMdT2EkDcQ9sDurBdnGK0QvF4kjauaZlTh8QujSvRcNeUh90zP4dNJLU0lNPYvEaRuKRl3NI3asTLuY6i5DGSqBJUEP8hScSej7Z0oAKlM0akOwCdBgFmVGloUbHKlmLzoYBo3gvJAnALUyCyJTQxbxNiitDFt3qjeox0Rz89+J8PayQSKIol4o4lloIKkVii6QSunUTB1GYo4kpQ1QVxL0I6i4flGvpq20NBllLdaBZI93GeHDfZaqI15D1YybpgyuhxgNeRmVaeo1UmCI8l8K4zyWlEryjLgV5Dx8r5dcL8hZRDch1QBGFsavKz0FGeUebSNbh9boej0nN+tc/Nh9n5Io4hWcwd7mI14GZdUzbqwELqmLTRjwryUks4R6jhtVNLXJ5i5ph+G3ri8PY2OWvbXHb1kxGvAtSvE0VlWTKXKUuqUqaC8MyYS7qhuF0c6kO+XPtinbqbqRty07c01RBVoFtJ7Y1eijLuom5dowruRPI+Plm1c5KOIaZTJC1XQ9hSzI34Q2M/hdPqaqRy8+X0pgrylkiyRZROc6JFiYLYCCER3B1GYPB2mtUQgmPuA2bfYLMhD3sfUcV9lLhmRcshCeR8SNRHIwIQ5ua6i2PtblOOJCHmVdOU5ynSAZzlDWrwyEKcd1lC5em/R1RbkOkPcnpxUOcBO4pEICtGRdUhHl1OEJ1WHKNVoJ71kIebnnlL4roxksUk8npOD0fgRwh0dFd521Ln8Yw9NCdYhD0q5WNfyMG6lqQhDNbCFGysI5aOkOeqvSWFLEUNpEIcnN+R8XcFkQhIyyR1EIEHSEIFn//2Q==").into(avatar);
        } else {
            Glide.with(this).load(Uri.parse(user.getAvatar())).into(avatar);
        }
        Log.d("Long", "---------------" + mPrefs.getString("avt_path",""));
        imv_button = (ImageView) view.findViewById(R.id.imv_btn);
        editText = (TextView) view.findViewById(R.id.txt_edit);
        myOrder = (TextView) view.findViewById(R.id.txt_my_order);
        logOut = (TextView) view.findViewById(R.id.txt_logout);
        tv_mail = (TextView) view.findViewById(R.id.tv_mail);
        tv_mail.setText(user.getEmail());
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_name.setText(user.getFullname());

        imv_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gallery = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                gallery.setType("image/*");

                startActivityForResult(Intent.createChooser(gallery, "Select picture"), PICK_IMAGE);
            }
        });
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intent);
            }
        });
        myOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListOrderActivity.class);
                startActivity(intent);
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.clear();
                editor.commit();
                openDialog();
                Log.d(TAG, "Now log out and start the activity login");

            }
        });
    }

    public void openDialog() {
        AlertDialog dialog = new AlertDialog.Builder(getActivity()).setTitle("Log out")
                .setMessage("Do you want to log out?")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                    }
                }).show();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.getMyInfo(view, user.getEmail());
    }

    //update user infor
    private void updateProfile(User newUser) {
        loadingDialog.startLoading();
        newUser.setId(this.user.getId());
        DocumentReference userRef = userCollection.document(user.getId());
        userRef.set(newUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Log.d(TAG, "Update profile successfully!");
                loadingDialog.dismissDialog();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w(TAG, "Error updating profile", e);
                loadingDialog.dismissDialog();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            Uri uri = data.getData();
            user.setAvatar(String.valueOf(uri));
            Glide.with(this).load(Uri.parse(user.getAvatar())).into(avatar);

            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString("avt_path",user.getAvatar());
            editor.commit();

            newUser = new User();
            newUser.setAvatar(user.getAvatar());
            newUser.setAddress(user.getAddress());
            newUser.setPhone(user.getPhone());
            newUser.setDOB(user.getDOB());
            newUser.setPassword(user.getPassword());
            newUser.setEmail(user.getEmail());
            newUser.setFullname(user.getFullname());
            newUser.setId(user.getId());
            updateProfile(newUser);
        }
    }

    private void setDialog(boolean show) {
        if (show) dialog.show();
        else dialog.dismiss();
    }
}