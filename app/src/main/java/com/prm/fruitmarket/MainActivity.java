package com.prm.fruitmarket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.prm.fruitmarket.firebase.model.User;
import com.prm.fruitmarket.fragment.CartFragment;
import com.prm.fruitmarket.fragment.HomeFragment;
import com.prm.fruitmarket.fragment.ProfileFragment;

import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("message");

    BottomNavigationView bottomNav;

    private TextView txtName;
    private User user;
    private CircleImageView avatar;
    private static final int PICK_IMAGE = 1;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myRef.setValue("Hello, Hieu!");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        txtName = findViewById(R.id.txtUserName);
        SharedPreferences mPrefs = getSharedPreferences("filename", MODE_PRIVATE);

        bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.getMenu().getItem(0).setChecked(true);
        getSupportFragmentManager().beginTransaction().replace(R.id.wrapper, new HomeFragment()).commit();
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        bottomNav.getMenu().getItem(0).setEnabled(false);
                        bottomNav.getMenu().getItem(1).setEnabled(true);
                        bottomNav.getMenu().getItem(2).setEnabled(true);
                        selectedFragment = new HomeFragment();
                        break;
                    case R.id.nav_cart:
                        bottomNav.getMenu().getItem(0).setEnabled(true);
                        bottomNav.getMenu().getItem(1).setEnabled(false);
                        bottomNav.getMenu().getItem(2).setEnabled(true);
                        selectedFragment = new CartFragment();
                        break;
                    case R.id.nav_profile:
                        bottomNav.getMenu().getItem(0).setEnabled(true);
                        bottomNav.getMenu().getItem(1).setEnabled(true);
                        bottomNav.getMenu().getItem(2).setEnabled(false);
                        selectedFragment = new ProfileFragment();
                        break;
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.wrapper, selectedFragment).commit();
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        return;
    }
}