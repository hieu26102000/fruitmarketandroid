package com.prm.fruitmarket.firebase.model;

import java.io.Serializable;

public class CartDetail implements Serializable {
    private Product product;
    private int quantity;

    public CartDetail(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public CartDetail() {

    }

    @Override
    public String toString() {
        return "CartDetail{" +
                "product=" + product +
                ", quantity=" + quantity +
                '}';
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
