package com.prm.fruitmarket.firebase.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Order implements Serializable {
    private ArrayList<OrderDetail> orderDetail;

    public Order(ArrayList<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    public Order() {

    }

    public ArrayList<OrderDetail> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(ArrayList<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }
}
