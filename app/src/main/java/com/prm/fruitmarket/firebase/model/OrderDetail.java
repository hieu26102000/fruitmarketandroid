package com.prm.fruitmarket.firebase.model;

import com.google.type.DateTime;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class OrderDetail implements Serializable {
    private float totalMoney;
    private int totalItem;
    private String createAt;
    private ArrayList<CartDetail> cartDetails;
    private String address;

    public OrderDetail(float totalMoney, int totalItem, ArrayList<CartDetail> cartDetails, String address) {
        this.totalMoney = totalMoney;
        this.totalItem = totalItem;
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
        this.createAt = df.format(Calendar.getInstance().getTime());
        this.cartDetails = cartDetails;
        this.address = address;
    }

    public OrderDetail() {

    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(float totalMoney) {
        this.totalMoney = totalMoney;
    }

    public int getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(int totalItem) {
        this.totalItem = totalItem;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public ArrayList<CartDetail> getCartDetails() {
        return cartDetails;
    }

    public void setCartDetails(ArrayList<CartDetail> cartDetails) {
        this.cartDetails = cartDetails;
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "totalMoney=" + totalMoney +
                ", totalItem=" + totalItem +
                ", createAt='" + createAt + '\'' +
                ", cartDetails=" + cartDetails +
                ", address='" + address + '\'' +
                '}';
    }
}
