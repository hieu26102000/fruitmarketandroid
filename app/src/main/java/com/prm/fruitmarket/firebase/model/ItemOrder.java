package com.prm.fruitmarket.firebase.model;

public class ItemOrder {
    private String date;
    private int item;
    private float total;

    public ItemOrder(int recourseId, String date, int item, float total) {
        this.date = date;
        this.item = item;
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
