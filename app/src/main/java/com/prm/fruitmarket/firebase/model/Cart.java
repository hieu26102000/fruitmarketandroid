package com.prm.fruitmarket.firebase.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Cart implements Serializable {
    private ArrayList<CartDetail> cartDeatil;
    private String email;

    public Cart(ArrayList<CartDetail> cartDeatil, String email) {
        this.cartDeatil = cartDeatil;
        this.email = email;
    }

    public Cart() {

    }

    public ArrayList<CartDetail> getCartDeatil() {
        return cartDeatil;
    }

    public void setCartDeatil(ArrayList<CartDetail> cartDeatil) {
        this.cartDeatil = cartDeatil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "cartDeatil=" + cartDeatil +
                ", email='" + email + '\'' +
                '}';
    }
}
