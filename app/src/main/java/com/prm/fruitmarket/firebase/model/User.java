package com.prm.fruitmarket.firebase.model;

public class User {
    private String id;
    private String email;
    private String fullname;
    private String password;
    private String DOB;
    private String phone;
    private String address;
    private String avatar;

    public User() {

    }

    public User(String email, String fullname, String password, String DOB, String phone, String address, String avatar) {
        this.email = email;
        this.fullname = fullname;
        this.password = password;
        this.DOB = DOB;
        this.phone = phone;
        this.address = address;
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
