package com.prm.fruitmarket.firebase.model;

import java.io.Serializable;
import java.util.List;

public class Product implements Serializable {
    private String id;
    private String name;
    private String description;
    private String origin;
    private float price;
    private int quantityInStock;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    private int quantitySold;
    private List<String> images;

    public Product() {

    }

    public Product(String name, List<String> images, String id) {
        this.name = name;
        this.images = images;
        this.id = id;
    }

    public Product(String name, String description, String origin, float price, int quantityInStock, int quantitySold, List<String> images) {
        this.name = name;
        this.description = description;
        this.origin = origin;
        this.price = price;
        this.quantityInStock = quantityInStock;
        this.quantitySold = quantitySold;
        this.images = images;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", quantityInStock=" + quantityInStock +
                ", quantitySold=" + quantitySold +
                ", images=" + images +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public int getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(int quantitySold) {
        this.quantitySold = quantitySold;
    }
}
