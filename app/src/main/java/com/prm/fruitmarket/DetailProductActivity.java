package com.prm.fruitmarket;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.prm.fruitmarket.firebase.model.Cart;
import com.prm.fruitmarket.firebase.model.CartDetail;
import com.prm.fruitmarket.firebase.model.Product;
import com.prm.fruitmarket.firebase.model.User;
import com.prm.fruitmarket.fragment.CartFragment;
import com.prm.fruitmarket.fragment.HomeFragment;

import java.util.ArrayList;

public class DetailProductActivity extends AppCompatActivity {

    private String TAG = "TAG";
    TextView textView;
    int numberQuantity = 1;
    ImageView btnAddNumber, btnMinusNumber, btnBack;
    private User user;

    private FirebaseFirestore db;
    private CollectionReference cartCollection;

    private SharedPreferences mPrefs;
    private LoadingDialog loadingDialog;
    TextView detailProductTitle, detailProductName, detailProductPrice, detailProductDescription, detailProductOrigin;
    Button btnAddToCart ;
    ImageView detailProductImage;
    Product product = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mPrefs = getSharedPreferences("filename", MODE_PRIVATE);
        loadingDialog = new LoadingDialog(DetailProductActivity.this);
        db = FirebaseFirestore.getInstance();
        cartCollection = db.collection("carts");

        Gson gson = new Gson();
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);



        final Object object = getIntent().getSerializableExtra("detailProduct");
        if (object instanceof Product) {
            product = (Product) object;
        }


        btnAddNumber = (ImageView) findViewById(R.id.btnAddNumber);
        btnMinusNumber = (ImageView) findViewById(R.id.btnMinusNumber);
        textView = (TextView) findViewById(R.id.textviewNumber);
        detailProductTitle = (TextView) findViewById(R.id.detailProductTitle);
        detailProductName = (TextView) findViewById(R.id.detailProductName);
        detailProductPrice = (TextView) findViewById(R.id.detailProductPrice);
        detailProductDescription = (TextView) findViewById(R.id.detailProductDescription);
        detailProductOrigin = (TextView) findViewById(R.id.detailProductOrigin);
        btnAddToCart = (Button) findViewById(R.id.btnAddToCart);
        detailProductImage = (ImageView) findViewById(R.id.detailProductImage);
        btnBack = (ImageView) findViewById(R.id.btnBack);

        if (product != null) {
            Glide.with(getApplicationContext()).load((product.getImages().get(0).toString())).override(200, 300).into(detailProductImage);
            detailProductTitle.setText(product.getName());
            detailProductName.setText(product.getName());
            detailProductPrice.setText(String.format("%.0f",product.getPrice()) + " VND");
            detailProductDescription.setText(product.getDescription());
            detailProductOrigin.setText("Origin: " + product.getOrigin());
        }

        String quantity = String.valueOf(numberQuantity);
        btnAddNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberQuantity++;
                String quantity = String.valueOf(numberQuantity);
                textView.setText(quantity);
            }
        });

        btnMinusNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numberQuantity <= 1) {
                    numberQuantity = 1;
                } else {
                    numberQuantity--;
                }

                String quantity = String.valueOf(numberQuantity);
                textView.setText(quantity);

            }
        });


        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                String img = intent.getStringExtra("images");
                String id = intent.getStringExtra("id");
                ArrayList<String> list = new ArrayList<>();
                list.add(img);

                CartDetail cartDetail = new CartDetail(product, numberQuantity);
                Log.d("checkQuantity", numberQuantity + "");
                addToCart(cartDetail);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailProductActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });



    }

    private void addToCart(CartDetail cartDetail) {
        loadingDialog.startLoading();
        DocumentReference cartRef = cartCollection.document(user.getEmail());
        cartRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        ArrayList<CartDetail> cartDetails = document.toObject(Cart.class).getCartDeatil();
                        Log.d(TAG, "Cart Detail " + cartDetails.size());
                        cartDetails.add(cartDetail);
                        Cart cart = new Cart(cartDetails, user.getEmail());
                        cartRef.set(cart)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        Toast.makeText(DetailProductActivity.this, "Add to cart successfully ", Toast.LENGTH_LONG).show();
                                        Log.d(TAG, "Add to cart successfully!");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        Log.w(TAG, "Error updating cart", e);
                                    }
                                });
                    } else {
                        ArrayList cartDetails = new ArrayList();
                        cartDetails.add(cartDetail);
                        Cart cart = new Cart(cartDetails, user.getEmail());

                        cartRef.set(cart)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(Task<Void> task) {
                                        Toast.makeText(DetailProductActivity.this, "Add to cart successfully ", Toast.LENGTH_LONG).show();
                                        Log.d(TAG, "Add new cart successfully for : " + user.getEmail());
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        Log.d(TAG, "Add new cart fail for : " + user.getEmail());
                                    }
                                });
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
                loadingDialog.dismissDialog();
            }
        });
    }


}