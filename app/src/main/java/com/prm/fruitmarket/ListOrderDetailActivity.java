package com.prm.fruitmarket;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.prm.fruitmarket.adapter.OrderDetailAdapter;
import com.prm.fruitmarket.firebase.model.CartDetail;
import com.prm.fruitmarket.firebase.model.Order;
import com.prm.fruitmarket.firebase.model.OrderDetail;
import com.prm.fruitmarket.firebase.model.Product;
import com.prm.fruitmarket.firebase.model.User;

import java.util.ArrayList;
import java.util.List;

public class ListOrderDetailActivity extends AppCompatActivity {
    private RecyclerView rcListvOrderDetail;
    private com.prm.fruitmarket.adapter.OrderDetailAdapter orderDetailAdapter;
    private TextView backPressed;

    private String TAG = "hieu";
    private FirebaseFirestore db;
    private CollectionReference orderDetailCollection;

    private LoadingDialog loadingDialog;
    private User user;
    SharedPreferences mPrefs;
    private OrderDetail orderDetail;
    private ArrayList<CartDetail> cartDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_order_detail);

        loadingDialog = new LoadingDialog(ListOrderDetailActivity.this);
        db = FirebaseFirestore.getInstance();
        orderDetailCollection = db.collection("orders");

        Gson gson = new Gson();
        mPrefs = getSharedPreferences("filename", Context.MODE_PRIVATE);
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);

        Intent i = getIntent();
        orderDetail = (OrderDetail) i.getSerializableExtra("orderDetail");
        Log.d("Haioc", "order : " + orderDetail.toString());

        backPressed = findViewById(R.id.tv_backPressedOrder);
        backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rcListvOrderDetail = findViewById(R.id.rcv_item_order_detail);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        getListOrderDetail();
        rcListvOrderDetail.setLayoutManager(linearLayoutManager);
        orderDetailAdapter = new OrderDetailAdapter(this,cartDetails);
        rcListvOrderDetail.setAdapter(orderDetailAdapter);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        rcListvOrderDetail.addItemDecoration(itemDecoration);
    }

    private void getListOrderDetail() {
        Intent i = getIntent();
        orderDetail = (OrderDetail) i.getSerializableExtra("orderDetail");
//        Log.d("Haioc", "order : " + orderDetail.toString());
        cartDetails = orderDetail.getCartDetails();
    }
}