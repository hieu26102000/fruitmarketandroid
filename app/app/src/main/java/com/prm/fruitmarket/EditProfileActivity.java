package com.prm.fruitmarket;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.prm.fruitmarket.firebase.model.User;

public class EditProfileActivity extends AppCompatActivity {

    private TextView txt_backPressed;
    private User user;
    private String TAG = "hieu";
    private FirebaseFirestore db;
    private CollectionReference userCollection;
    private LoadingDialog loadingDialog;
    private SharedPreferences mPrefs;
    private EditText edt_name, edt_mail, edt_password, edt_dob, edt_phone, edt_address;
    private Button btn_change;
    private User newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);


        mPrefs = getSharedPreferences("filename", MODE_PRIVATE);
        loadingDialog = new LoadingDialog(EditProfileActivity.this);
        db = FirebaseFirestore.getInstance();
        userCollection = db.collection("users");

        Gson gson = new Gson();
        String json = mPrefs.getString("USER", "");
        user = gson.fromJson(json, User.class);
        Log.d(TAG, "user" + user.getId().toString());

//        fill data into view
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_mail = (EditText) findViewById(R.id.edt_mail);
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_dob = (EditText) findViewById(R.id.edt_dob);
        edt_phone = (EditText) findViewById(R.id.edt_phone);
        edt_address = (EditText) findViewById(R.id.edt_address);

        showAllUserData();
//        click event button change
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserData(newUser);
                updateProfile(newUser);
            }
        });

        txt_backPressed = findViewById(R.id.txt_backPressed);
        txt_backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    //show user data
    private void showAllUserData() {
        edt_name.setText(user.getFullname().trim());
        edt_mail.setText(user.getEmail().trim());
        edt_password.setText(user.getPassword().trim());
        edt_dob.setText(user.getDOB().trim());
        edt_phone.setText(user.getPhone().trim());
        edt_address.setText(user.getAddress().trim());
    }

    //save new user data
    private void saveUserData(User user){

        newUser = new User();

        newUser.setFullname(edt_name.getText().toString());
        newUser.setEmail(edt_mail.getText().toString());
        newUser.setPassword(edt_password.getText().toString());
        newUser.setDOB(edt_dob.getText().toString());
        newUser.setPhone(edt_phone.getText().toString());
        newUser.setAddress(edt_address.getText().toString());
    }

    //update user infor
    private void updateProfile(User newUser) {
        loadingDialog.startLoading();
        newUser.setId(this.user.getId());
        DocumentReference userRef = userCollection.document(user.getId());
        userRef.set(newUser).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Log.d(TAG, "Update profile successfully!");
                loadingDialog.dismissDialog();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w(TAG, "Error updating profile", e);
                loadingDialog.dismissDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}