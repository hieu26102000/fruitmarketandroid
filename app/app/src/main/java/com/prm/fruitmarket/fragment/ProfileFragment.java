package com.prm.fruitmarket.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.prm.fruitmarket.EditProfileActivity;
import com.prm.fruitmarket.ListOrderActivity;
import com.prm.fruitmarket.MainActivity;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.firebase.model.User;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int PICK_IMAGE = 1;
    private User user;
    private String TAG = "hieu";
    private SharedPreferences mPrefs;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CircleImageView avatar;
    private ImageView imv_button;
    private TextView editText, myOrder, logOut, tv_name, tv_mail;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = getActivity().getSharedPreferences("filename", Context.MODE_PRIVATE);
//        Gson gson = new Gson();
//        String json = mPrefs.getString("USER", "");
//        user = gson.fromJson(json, User.class);

        user = new User(
                "hieu26102000@gmail.com",
                "Nguyen Van Hieu",
                "hieu123",
                "26/10/2000",
                "09656165xx",
                "test123",
                "avatar");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        avatar = (CircleImageView) view.findViewById(R.id.imv_avatar);
        Glide.with(this).load(Uri.parse(mPrefs.getString("avatar_path",""))).into(avatar);
        imv_button = (ImageView) view.findViewById(R.id.imv_btn);
        editText = (TextView) view.findViewById(R.id.txt_edit);
        myOrder = (TextView) view.findViewById(R.id.txt_my_order);
        logOut = (TextView) view.findViewById(R.id.txt_logout);
        tv_mail = (TextView) view.findViewById(R.id.tv_mail);
        tv_mail.setText(user.getEmail());
        tv_name=(TextView) view.findViewById(R.id.tv_name);
        tv_name.setText(user.getFullname());
        imv_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gallery = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                gallery.setType("image/*");

                startActivityForResult(Intent.createChooser(gallery, "Select picture"), PICK_IMAGE);
            }
        });
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intent);
            }
        });
        myOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListOrderActivity.class);
                startActivity(intent);
            }
        });

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            Uri uri = data.getData();
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString("avatar_path", String.valueOf(uri));
            editor.commit();
            Glide.with(this).load(Uri.parse(mPrefs.getString("avatar_path",""))).into(avatar);
        }
    }
}