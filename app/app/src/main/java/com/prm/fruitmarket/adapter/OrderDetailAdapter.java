package com.prm.fruitmarket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.prm.fruitmarket.R;
import com.prm.fruitmarket.firebase.model.OrderDetail;
import com.prm.fruitmarket.firebase.model.Product;

import java.util.List;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderDetailViewHolder>{

    private Context context;
    private List<Product> mProduct;

    public OrderDetailAdapter(Context context, List<Product> mProduct) {
        this.context = context;
        this.mProduct = mProduct;
    }

    @NonNull
    @Override
    public OrderDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_order,parent,false);
        return new OrderDetailAdapter().OrderDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderDetailAdapter.OrderDetailViewHolder holder, int position) {
        Product product = mProduct.get(position);
        if (product == null){
            return;
        }
        Glide.with(context).load(product.).into(holder.img_product);
        holder.tv_item.setText("Item: " + String.valueOf(orderDetail.getTotalItem()));
        holder.tv_total.setText("Total: " + String.valueOf(orderDetail.getTotalMoney()));
    }

    @Override
    public int getItemCount() {
        if (mProduct !=null){
            return mProduct.size();
        }
        return 0;
    }

    public class OrderDetailViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_product;
        private TextView tv_pd_name,tv_price;
        public OrderDetailViewHolder(View itemView) {
            super(itemView);
            img_product = itemView.findViewById(R.id.img_product);
            tv_pd_name = itemView.findViewById(R.id.tv_pd_name);
            tv_price = itemView.findViewById(R.id.tv_price);
        }
    }
}
