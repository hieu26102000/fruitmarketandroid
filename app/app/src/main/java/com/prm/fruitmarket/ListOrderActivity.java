package com.prm.fruitmarket;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.prm.fruitmarket.firebase.model.Cart;
import com.prm.fruitmarket.firebase.model.CartDetail;
import com.prm.fruitmarket.firebase.model.Order;
import com.prm.fruitmarket.firebase.model.OrderDetail;
import com.prm.fruitmarket.firebase.model.User;

import java.util.ArrayList;
import java.util.List;

public class ListOrderActivity extends AppCompatActivity {

    private RecyclerView rcvItemOrder;
    private ItemOrderAdapter ItemOrderAdapter;
    private TextView backPressed;

    private String TAG = "hieu";
    private FirebaseFirestore db;
    private CollectionReference orderCollection;

    private LoadingDialog loadingDialog;
    private User user;
    SharedPreferences mPrefs;
    private ArrayList<Order> orders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_order);

        loadingDialog = new LoadingDialog(ListOrderActivity.this);
        db = FirebaseFirestore.getInstance();
        orderCollection = db.collection("orders");

//        Gson gson = new Gson();
//        String json = mPrefs.getString("USER", "");
//        user = gson.fromJson(json, User.class);

        user = new User(
                "hieu26102000@gmail.com",
                "Nguyen Van Hieu",
                "hieu123",
                "26/10/2000",
                "09656165xx",
                "test123",
                "avatar");

        backPressed = findViewById(R.id.tv_backPressedOrder);
        backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rcvItemOrder = findViewById(R.id.rcv_item_order);
        ItemOrderAdapter = new ItemOrderAdapter(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rcvItemOrder.setLayoutManager(linearLayoutManager);

//        ItemOrderAdapter.setData(getListOrder());
        rcvItemOrder.setAdapter(ItemOrderAdapter);

    }

//    private List<ItemOrder> getListOrder() {
//        List<ItemOrder> list = new ArrayList<>();
//        list.add(new ItemOrder(R.drawable.orange_demo, "11/04/2000"));
//        list.add(new ItemOrder(R.drawable.orange_demo, "11/05/2000"));
//        list.add(new ItemOrder(R.drawable.orange_demo, "11/06/2000"));
//        list.add(new ItemOrder(R.drawable.orange_demo, "11/07/2000"));
//        return list;
//    }

    private void getListOrder() {
        loadingDialog.startLoading();
        DocumentReference orderRef = orderCollection.document(user.getEmail());
        orderRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        ArrayList<OrderDetail> orderDetails = document.toObject(Order.class).getOrderDetail();
                        // set data len view
                        Log.d(TAG, "Get orders successfully !" + orderDetails.size());
                    } else {
                        Log.d(TAG, "There are no orders!");
                    }
                } else {
                    Log.d(TAG, "get cart fail ", task.getException());
                }
                loadingDialog.dismissDialog();
            };
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}